package com.yht.dao;

import com.yht.pojo.TeaUser;
import com.yht.pojo.Users;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TeaUserDao {
    TeaUser teaLogin(String username, String userpwd);
    //获取教师姓名
    String getTeaTruename(String username);
    //获取教师班级id
    Integer getClassId(String username);
    //获取教师id
    Integer getUserid(String username);
}
