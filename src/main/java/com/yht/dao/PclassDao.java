package com.yht.dao;

import com.yht.pojo.Pclass;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PclassDao {
    //查询班级列表
    List<Pclass> getAllPclass();
    //查询班级id
    Pclass GetClassId(Integer classid);
}
