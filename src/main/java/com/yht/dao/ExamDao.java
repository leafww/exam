package com.yht.dao;

import com.yht.pojo.Exam;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ExamDao {
    //添加考试
    Integer addExam(Exam exam);
    //查询所有考试
    List<Exam> getAllExam();
    //根据id查询
    Exam getExam(Integer eid);
    //修改考试信息
    Integer updExam(Exam exam);
    //删除考试
    Integer delExam(Integer eid);
    //通过班级id查询考试信息
    List<Exam> getExamClassid(Integer classid);


}
