package com.yht.dao;

import com.yht.pojo.StudentExam;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StudentExamDao {
    //添加学生成绩
    Integer addStudentExam(StudentExam studentExam);
    //查询学生成绩
    List<StudentExam> getStuPaperList(Integer userid);
    //查询是否做过该试卷
    List<StudentExam> getIsPaper(Integer userid,Integer eid);
    //通过教师id查询试卷信息、成绩
    List<StudentExam> getAllStuScore(Integer classid);

    //无法删除的BUG之清空考试信息1
    Integer deleteByUser(Integer userid);
    //无法删除的BUG之清空考试信息2
    Integer deleteEid(Integer eid);

}
