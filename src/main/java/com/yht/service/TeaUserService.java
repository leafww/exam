package com.yht.service;

import com.yht.pojo.Pclass;
import com.yht.pojo.TeaUser;

public interface TeaUserService {
    TeaUser teaLogin(String username, String userpwd);
    String getTeaTruename(String username);
    //查询教师的班级id
    Integer getClassId(String username);
    Integer getUserid(String username);

}
