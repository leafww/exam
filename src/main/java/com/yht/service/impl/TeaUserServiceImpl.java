package com.yht.service.impl;

import com.yht.dao.PclassDao;
import com.yht.dao.TeaUserDao;
import com.yht.pojo.Pclass;
import com.yht.pojo.TeaUser;
import com.yht.service.TeaUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeaUserServiceImpl implements TeaUserService {
    @Autowired
    TeaUserDao teauserDao;

    @Override
    public TeaUser teaLogin(String username, String userpwd) {
        return teauserDao.teaLogin(username,userpwd);
    }

    @Override
    public String getTeaTruename(String username) {
        return teauserDao.getTeaTruename(username);
    }

    @Override
    public Integer getClassId(String username) {
        return teauserDao.getClassId(username);
    }

    @Override
    public Integer getUserid(String username) {
        return teauserDao.getUserid(username);
    }
}
