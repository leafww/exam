package com.yht.service.impl;

import com.yht.dao.StudentExamDao;
import com.yht.dao.UsersDao;
import com.yht.pojo.Users;
import com.yht.service.StudentExamService;
import com.yht.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService{

    @Autowired
    UsersDao usersDao;
    @Autowired
    StudentExamDao studentExamDao;
    @Override
    public Users login(String username, String userpwd) {
        return usersDao.login(username, userpwd);
    }

    @Override
    public Integer addUsers(Users users) {
        return usersDao.addUsers(users);
    }

    @Override
    public Users getByName(String username) {
        return usersDao.getByName(username);
    }

    @Override
    public List<Users> pageById(Integer classid) {
        return usersDao.pageById(classid);
    }

    @Override
    public Integer updateUser(Users users) {
        return usersDao.updateUser(users);
    }

    @Override
    public Users getByUserid(Integer Users) {
        return usersDao.getByUserid(Users);
    }

    @Override
    public Integer delUserid(Integer userid) {
        studentExamDao.deleteByUser(userid);
        return usersDao.delUserid(userid);
    }

    @Override
    public Integer delClassid(Integer classid) {
        List<Users> allUserid = usersDao.getAllUserid(classid);
        for(Users users : allUserid){
            Integer userid1 = users.getUserid();
            studentExamDao.deleteByUser(userid1);
        }
        return usersDao.delClassid(classid);
    }

    @Override
    public Integer updUserPwd(String userpwd, Integer userid) {
        return usersDao.updUserPwd(userpwd,userid);
    }

    @Override
    public Integer getByNameId(String username) {
        return usersDao.getByNameId(username);
    }

    @Override
    public String getTrueName(String username) {
        return usersDao.getTrueName(username);
    }

    @Override
    public Integer getByClass(String username) {
        return usersDao.getByClass(username);
    }

    @Override
    public List<Users> getAllUserid(Integer classid) {
        return usersDao.getAllUserid(classid);
    }
}
