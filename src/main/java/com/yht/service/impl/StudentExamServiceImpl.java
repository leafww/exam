package com.yht.service.impl;

import com.yht.dao.StudentExamDao;
import com.yht.pojo.StudentExam;
import com.yht.service.StudentExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentExamServiceImpl implements StudentExamService {
    @Autowired
    StudentExamDao studentExamDao;

    @Override
    public Integer addStudentExam(StudentExam studentExam) {
        return studentExamDao.addStudentExam(studentExam);
    }

    @Override
    public List<StudentExam> getStuPaperList(Integer userid) {
        return studentExamDao.getStuPaperList(userid);
    }

    @Override
    public List<StudentExam> getIsPaper(Integer userid, Integer eid) {
        return studentExamDao.getIsPaper(userid,eid);
    }

    @Override
    public List<StudentExam> getAllStuScore(Integer classid) {
        return studentExamDao.getAllStuScore(classid);
    }

    @Override
    public Integer deleteByUser(Integer userid) {
        return studentExamDao.deleteByUser(userid);
    }

    @Override
    public Integer deleteEid(Integer eid) {
        return studentExamDao.deleteEid(eid);
    }
}
