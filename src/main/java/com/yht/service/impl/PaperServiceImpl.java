package com.yht.service.impl;

import com.yht.dao.PaperDao;
import com.yht.pojo.Paper;
import com.yht.service.PaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaperServiceImpl implements PaperService {
    @Autowired
    PaperDao paperDao;

    @Override
    public Integer addPaper(Paper paper) {
        return paperDao.addPaper(paper);
    }

    @Override
    public List<Paper> getByEid(Integer eid) {
        return paperDao.getByEid(eid);
    }

    @Override
    public Integer deleteByEid(Integer eid) {
        return paperDao.deleteByEid(eid);
    }
}
