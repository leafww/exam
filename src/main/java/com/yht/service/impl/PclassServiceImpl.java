package com.yht.service.impl;

import com.yht.dao.PclassDao;
import com.yht.pojo.Pclass;
import com.yht.service.PclassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PclassServiceImpl implements PclassService{
    @Autowired
    PclassDao pclassDao;

    @Override
    public List<Pclass> getAllPclass() {
        return pclassDao.getAllPclass();
    }

    @Override
    public Pclass getClassId(Integer classid) {
        return pclassDao.GetClassId(classid);
    }
}
