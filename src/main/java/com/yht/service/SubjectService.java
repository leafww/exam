package com.yht.service;

import com.yht.pojo.Subject;

import java.util.List;

public interface SubjectService {
    List<Subject> getAllSubject();
    Integer addSubject(Subject subject);
    Integer updateSingle(Subject subject);
    Subject getBySid(Integer sid);
    Integer delSingle(Integer sid);
    List<Subject> getSubject(Integer cno);


}
