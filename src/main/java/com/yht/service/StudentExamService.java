package com.yht.service;

import com.yht.pojo.StudentExam;

import java.util.List;

public interface StudentExamService {
    Integer addStudentExam(StudentExam studentExam);
    List<StudentExam> getStuPaperList(Integer userid);
    List<StudentExam> getIsPaper(Integer userid,Integer eid);
    List<StudentExam> getAllStuScore(Integer classid);
    Integer deleteByUser(Integer userid);
    Integer deleteEid(Integer eid);

}
