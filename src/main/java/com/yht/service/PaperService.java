package com.yht.service;

import com.yht.pojo.Paper;

import java.util.List;

public interface PaperService {
    Integer addPaper(Paper paper);
    List<Paper> getByEid(Integer eid);
    Integer deleteByEid(Integer eid);

}
