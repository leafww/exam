package com.yht.service;

import com.yht.pojo.Course;

import java.util.List;

public interface CourseService {
    Course getAllById(Integer cno);
    List<Course> getAllCourse();


}
