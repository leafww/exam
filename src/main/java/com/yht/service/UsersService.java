package com.yht.service;

import com.yht.pojo.Users;


import java.util.List;

public interface UsersService {
    Users login(String username, String userpwd);
    Integer addUsers(Users users);
    Users getByName(String username);
    List<Users> pageById(Integer classid);
    Integer updateUser(Users users);
    Users getByUserid(Integer Users);
    Integer delUserid(Integer userid);
    Integer delClassid(Integer classid);
    Integer updUserPwd(String userpwd,Integer userid);
    Integer getByNameId(String username);
    String getTrueName(String username);
    Integer getByClass(String username);
    List<Users> getAllUserid(Integer classid);


}
