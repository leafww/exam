package com.yht.service;

import com.yht.pojo.Pclass;

import java.util.List;

public interface PclassService {
    List<Pclass> getAllPclass();
    //查询班级的id
    Pclass getClassId(Integer classid);
}
