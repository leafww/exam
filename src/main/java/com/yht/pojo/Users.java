package com.yht.pojo;

public class Users {
    private Integer userid;
    private String username;
    private String userpwd;
    private String truename;
    private Integer classid;

    public Users() {
    }

    public Users(Integer userid, String username, String userpwd, String truename, Integer classid) {
        this.userid = userid;
        this.username = username;
        this.userpwd = userpwd;
        this.truename = truename;
        this.classid = classid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpwd() {
        return userpwd;
    }

    public void setUserpwd(String userpwd) {
        this.userpwd = userpwd;
    }

    public String getTruename() {
        return truename;
    }

    public void setTruename(String truename) {
        this.truename = truename;
    }

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

    @Override
    public String toString() {
        return "Users{" +
                "userid=" + userid +
                ", username='" + username + '\'' +
                ", userpwd='" + userpwd + '\'' +
                ", truename='" + truename + '\'' +
                ", classid=" + classid +
                '}';
    }
}
