package com.yht.pojo;

import java.util.Date;

public class Exam {
    private Integer eid;
    private String pname;
    private Integer cno;
    private Integer userid;
    private Integer classid;
    private Integer singlenumber;
    private Integer singlecore;
    private Date examdate;
    private Date examtime;
    private Integer testtime;
    private Course course;

    public Exam() {
    }

    public Exam(Integer eid, String pname, Integer cno, Integer userid, Integer classid, Integer singlenumber, Integer singlecore, Date examdate, Date examtime, Integer testtime, Course course) {
        this.eid = eid;
        this.pname = pname;
        this.cno = cno;
        this.userid = userid;
        this.classid = classid;
        this.singlenumber = singlenumber;
        this.singlecore = singlecore;
        this.examdate = examdate;
        this.examtime = examtime;
        this.testtime = testtime;
        this.course = course;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public Integer getCno() {
        return cno;
    }

    public void setCno(Integer cno) {
        this.cno = cno;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

    public Integer getSinglenumber() {
        return singlenumber;
    }

    public void setSinglenumber(Integer singlenumber) {
        this.singlenumber = singlenumber;
    }

    public Integer getSinglecore() {
        return singlecore;
    }

    public void setSinglecore(Integer singlecore) {
        this.singlecore = singlecore;
    }

    public Date getExamdate() {
        return examdate;
    }

    public void setExamdate(Date examdate) {
        this.examdate = examdate;
    }

    public Date getExamtime() {
        return examtime;
    }

    public void setExamtime(Date examtime) {
        this.examtime = examtime;
    }

    public Integer getTesttime() {
        return testtime;
    }

    public void setTesttime(Integer testtime) {
        this.testtime = testtime;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Exam{" +
                "eid=" + eid +
                ", pname='" + pname + '\'' +
                ", cno=" + cno +
                ", userid=" + userid +
                ", classid=" + classid +
                ", singlenumber=" + singlenumber +
                ", singlecore=" + singlecore +
                ", examdate=" + examdate +
                ", examtime=" + examtime +
                ", testtime=" + testtime +
                ", course=" + course +
                '}';
    }
}
