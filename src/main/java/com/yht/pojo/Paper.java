package com.yht.pojo;

public class Paper {
    private Integer pid;
    private Integer eid;
    private Integer sid;
    private Integer cno;
    private String scontent;
    private String sa;
    private String sb;
    private String sc;
    private String sd;
    private String skey;

    public Paper() {
    }

    public Paper(Integer pid, Integer eid, Integer sid, Integer cno, String scontent, String sa, String sb, String sc, String sd, String skey) {
        this.pid = pid;
        this.eid = eid;
        this.sid = sid;
        this.cno = cno;
        this.scontent = scontent;
        this.sa = sa;
        this.sb = sb;
        this.sc = sc;
        this.sd = sd;
        this.skey = skey;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public Integer getCno() {
        return cno;
    }

    public void setCno(Integer cno) {
        this.cno = cno;
    }

    public String getScontent() {
        return scontent;
    }

    public void setScontent(String scontent) {
        this.scontent = scontent;
    }

    public String getSa() {
        return sa;
    }

    public void setSa(String sa) {
        this.sa = sa;
    }

    public String getSb() {
        return sb;
    }

    public void setSb(String sb) {
        this.sb = sb;
    }

    public String getSc() {
        return sc;
    }

    public void setSc(String sc) {
        this.sc = sc;
    }

    public String getSd() {
        return sd;
    }

    public void setSd(String sd) {
        this.sd = sd;
    }

    public String getSkey() {
        return skey;
    }

    public void setSkey(String skey) {
        this.skey = skey;
    }

    @Override
    public String toString() {
        return "Paper{" +
                "pid=" + pid +
                ", eid=" + eid +
                ", sid=" + sid +
                ", cno=" + cno +
                ", scontent='" + scontent + '\'' +
                ", sa='" + sa + '\'' +
                ", sb='" + sb + '\'' +
                ", sc='" + sc + '\'' +
                ", sd='" + sd + '\'' +
                ", skey='" + skey + '\'' +
                '}';
    }
}
