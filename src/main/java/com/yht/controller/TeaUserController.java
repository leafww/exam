package com.yht.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yht.pojo.*;
import com.yht.service.*;
import org.apache.ibatis.javassist.bytecode.LineNumberAttribute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import sun.plugin2.gluegen.runtime.CPU;

import javax.jws.WebParam;
import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Controller
public class TeaUserController {
    @Autowired
    TeaUserService teaUserService;
    @Autowired
    PclassService pclassService;
    @Autowired
    UsersService usersService;
    @Autowired
    SubjectService subjectService;
    @Autowired
    CourseService courseService;
    @Autowired
    ExamService examService;
    @Autowired
    PaperService paperService;
    @Autowired
    StudentExamService studentExamService;

    //教师登录
    @PostMapping("/tealogin")
    public String teaLogin(String username, String userpwd, HttpServletRequest request){
        //查询班级ID
        Integer classId = teaUserService.getClassId(username);
        //获取教师id
        Integer userid = teaUserService.getUserid(username);

        TeaUser teaLogin = teaUserService.teaLogin(username, userpwd);

        if (teaLogin == null){
            System.out.println("登陆失败");
            request.getSession().setAttribute("msg","密码错误");
            return "redirect:/toTeacher";
        }else {
            String teaTruename = teaUserService.getTeaTruename(username);
            request.getSession().setAttribute("Teatruename",teaTruename);
            request.getSession().setAttribute("TeaClassid",classId);
            request.getSession().setAttribute("Teauserid",userid);
            System.out.println("登陆成功");
            return "teacher/manage";
        }
    }
    //查询学生列表
    @GetMapping("/StudentList")
    public String findStuUsers(HttpServletRequest request, Model model, @RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum){
        //获取班级id
        Integer teaClassid = (Integer) request.getSession().getAttribute("TeaClassid");
        //查看教师所在班级
        Pclass classid = pclassService.getClassId(teaClassid);
        model.addAttribute("pj",classid);

        //设置分页
        PageHelper.startPage(pageNum,5);
        //查询学生列表
        List<Users> users = usersService.pageById(teaClassid);
        PageInfo<Users> usersPageInfo = new PageInfo<>(users);
        model.addAttribute("pageInfo",usersPageInfo);
        model.addAttribute("liss",users);

        //获取班级所有信息
        List<Pclass> allPclass = pclassService.getAllPclass();
        model.addAttribute("list",allPclass);
        return "teacher/StudentList";
    }

    //添加学生
    @PostMapping("/addStu")
    public String addStuUsers(String username,String userpwd,String truename,Integer classid){
        //查询学生是否存在
        Users byName = usersService.getByName(username);
        Users users = new Users();
        if(byName==null){
            users.setUsername(username);
            users.setUserpwd(userpwd);
            users.setTruename(truename);
            users.setClassid(classid);
            usersService.addUsers(users);
        }
        else {
            System.out.println("该学生已存在");
        }
        return "redirect:/StudentList";
    }

    //查询所有班级
    @ResponseBody
    @PostMapping("/findAllClass")
    public List<Pclass> pclassList(){
        List<Pclass> allPclass = pclassService.getAllPclass();
        return allPclass;
    }
    //获取学生信息
    @ResponseBody
    @PostMapping("/StuEdit")
    public Users stuEdit(@RequestBody Users users){
        Users byUserid = usersService.getByUserid(users.getUserid());
        if(byUserid!=null){
            return byUserid;
        }
        else{
            return null;
        }
    }
    //修改学生
    @PostMapping("/updateStu")
    public String updStuUser(Users users){
        usersService.updateUser(users);
        return "redirect:/StudentList";
    }

    //删除学生
    @GetMapping("/DeleteStu")
    public String deleteUsers(HttpServletRequest Request){
        Integer userid = Integer.valueOf(Request.getParameter("userid"));
        usersService.delUserid(userid);
        return "redirect:/StudentList";
    }

    //批量删除
    @GetMapping("/deleteAll")

    public String deleteUserAll(HttpServletRequest request){
        Integer teaClassid = (Integer) request.getSession().getAttribute("TeaClassid");
        usersService.delClassid(teaClassid);
        return "redirect:/StudentList";
    }

    //查询题目
    @GetMapping("/finddanxuan")
    public String findSingle(@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum, Model model){
        PageHelper.startPage(pageNum,5);
        List<Subject> allSubject = subjectService.getAllSubject();
        for(Subject subject:allSubject){
            Course allById = courseService.getAllById(subject.getCno());
            subject.setCourse(allById);
        }
        PageInfo<Subject> SubjectPageInfo = new PageInfo<>(allSubject);
        model.addAttribute("pageInfo",SubjectPageInfo);
        model.addAttribute("subjectlist",allSubject);
        return "teacher/Single";
    }

    //获取课程
    @ResponseBody
    @GetMapping("/findAllCourse")
    public List<Course> coursesList(){
        List<Course> allCourse = courseService.getAllCourse();

        return allCourse;
    }
    //添加题
    @PostMapping("/addSingle")
    public String addSingle(Integer cno,String scontent,String sa,String sb,String sc,String sd,String skey){
        Subject subject = new Subject();
        subject.setCno(cno);
        subject.setScontent(scontent);
        subject.setSa(sa);
        subject.setSb(sb);
        subject.setSc(sc);
        subject.setSd(sd);
        subject.setSkey(skey);
        subjectService.addSubject(subject);
        return "redirect:/finddanxuan";
    }

    //获取题目数据
    @ResponseBody
    @PostMapping("/findBySid")
    public Subject findBySid(@RequestBody Subject subject){
        Subject subject1 = subjectService.getBySid(subject.getSid());
        if(subject1!=null){
            return subject1;
        }
        else{
            return null;
            }
    }
    //修改题目
    @PostMapping("/updateSingle")
    public String updSingle(Subject subject){
        subjectService.updateSingle(subject);
        return "redirect:/finddanxuan";
    }

    //删除题目
    @PostMapping("/deleteSingle")
    public String delSingle(@RequestParam Integer sid){
        subjectService.delSingle(sid);
        return "redirect:/finddanxuan";
    }

    //查询考试信息
    @GetMapping("/selectexam")
    public String selectExam(@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum, Model model){
        PageHelper.startPage(pageNum,5);
        List<Exam> allExam = examService.getAllExam();
        PageInfo<Exam> examPageInfo = new PageInfo<>(allExam);
        model.addAttribute("examlist",allExam);
        model.addAttribute("pageInfo",examPageInfo);
        return "teacher/SelectExam";
    }

    //跳转到考试的页面的方法
    @GetMapping("/addexam")
    public String toAddExam(){
        return "teacher/addexam";
    }

    //创建考试
    @PostMapping("/addexames")
    public String addExam(String pname, Integer userid, Integer cno, Integer classid, Integer singlenumber, Integer singlecore, Date examdate,Date examtime,Integer testtime){
        //生成考试信息
        Exam exam = new Exam();
        exam.setPname(pname);
        exam.setUserid(userid);
        exam.setCno(cno);
        exam.setClassid(classid);
        exam.setSinglenumber(singlenumber);
        exam.setSinglecore(singlecore);
        exam.setExamdate(examdate);
        exam.setExamtime(examtime);
        exam.setTesttime(testtime);
        examService.addExam(exam);

        //通过课程查询
        List<Subject> subject = subjectService.getSubject(cno);
        //题的随机生成
        ArrayList<Subject> subjects = new ArrayList<>();
        Random random = new Random();
        if(singlenumber>0 && subject.size() >0){
            for(int i = 1;i<=singlenumber;i++){
                int s = random.nextInt(subject.size());

                Subject subject1 = subject.get(s);

                if(subjects.contains(subject1)){
                    i--;
                } else {
                    subjects.add(subject.get(s));
                    Paper paper = new Paper();
                    paper.setEid(exam.getEid());
                    paper.setCno(cno);
                    paper.setSid(subject1.getSid());
                    paper.setScontent(subject1.getScontent());
                    paper.setSa(subject1.getSa());
                    paper.setSb(subject1.getSb());
                    paper.setSc(subject1.getSc());
                    paper.setSd(subject1.getSd());
                    paper.setSkey(subject1.getSkey());
                    paperService.addPaper(paper);
                }
                if (i == singlenumber) {
                    break; // 当生成了所需的题目数量时，退出循环
                }
            }
        }
        return "redirect:/selectexam";
    }

    //查询试卷信息
    @GetMapping("/paperDetails")
    public String paperDetails(Integer eid,Model model){
    List<Paper> byEid = paperService.getByEid(eid);
    model.addAttribute("tm",byEid);
    Exam exam = examService.getExam(eid);
    model.addAttribute("exam",exam);

        return "teacher/paperDetails";
    }

    //查询考试信息
    @ResponseBody
    @PostMapping("/findByOneExam")
    public Exam findByOneExam(@RequestBody Exam exam){
        Exam exam1 = examService.getExam(exam.getEid());
        if(exam1==null){
            return null;
        }
        else{
            return exam1;
        }
    }

    //获取所有班级
    @ResponseBody
    @GetMapping("/findAllClasses")
    public List<Pclass> findAllClasses(){
        return pclassService.getAllPclass();
    }

    //修改考试信息
    @PostMapping("/updateExam")
    public String updateExam(Exam exam){
        examService.updExam(exam);
        return "redirect:/selectexam";
//        return "teacher/paperDetails";

    }

    //删除考试信息
    @GetMapping("deleteExam")
    public String deleteExam(Integer eid){
        examService.delExam(eid);
        return "redirect:/selectexam";
    }

    //教师退出登录
    @GetMapping("/Tlogout")
    public String Tlogout(HttpServletRequest request){
        request.getSession().removeAttribute("Teauserid");
        request.getSession().removeAttribute("TeaClassid");
        request.getSession().removeAttribute("Teatruename");
        return "redirect:/toTeacher";
    }

    //查询学生成绩
    @GetMapping("/findAllScore")
    public String findAllScore(@RequestParam(defaultValue = "1",value = "pageNum")Integer pageNum, HttpServletRequest request,Model model){
        PageHelper.startPage(pageNum,5);
        //获取教师班级id
        Integer teaClassid = (Integer) request.getSession().getAttribute("TeaClassid");
        Pclass classes = pclassService.getClassId(teaClassid);
        model.addAttribute("cs",classes);
        List<StudentExam> allStuScore = studentExamService.getAllStuScore(teaClassid);
        for(StudentExam studentExam : allStuScore){
            Users byUserid = usersService.getByUserid(studentExam.getUserid());
            studentExam.setUsers(byUserid);
        }
        PageInfo<StudentExam> studentExamPageInfo = new PageInfo<>(allStuScore);

        model.addAttribute("pageInfo",studentExamPageInfo);
        model.addAttribute("score",allStuScore);
        return "teacher/studentScore";
    }
}
