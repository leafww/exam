package com.yht.controller;

import com.yht.pojo.Pclass;
import com.yht.pojo.Users;
import com.yht.service.PclassService;
import com.yht.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class UsersController {
    @Autowired
    UsersService usersService;

    //教师登陆与学生登陆的互相跳转
    @GetMapping("toTeacher")
    public String toTeaLogin(){
        return "teacher/login";
    }
    @GetMapping("toStuLogin")
    public String toStuLogin(){
        return "student/login";
    }

    //学生登陆
    @PostMapping("/stulogin")
    public String stulogin(String username, String userpwd, HttpServletRequest servletRequest){
        Users login = usersService.login(username, userpwd);

        if (login==null){
            System.out.println("登陆失败");
            servletRequest.getSession().setAttribute("mag","密码错误");
            return "redirect:/toStuLogin";
        }else {
            Integer userid = usersService.getByNameId(username);

            String trueName = usersService.getTrueName(username);

            Integer byClass = usersService.getByClass(username);

            System.out.println("登陆成功");
            servletRequest.getSession().setAttribute("liss",userpwd);

            servletRequest.getSession().setAttribute("truename",trueName);

            servletRequest.getSession().setAttribute("lis",userid);

            servletRequest.getSession().setAttribute("classid",byClass);

            return "student/StuMan";
        }

    }

    //注册
    @Autowired
    PclassService pclassService;
    @GetMapping("/register")
    public String toRegister(Model model){
        List<Pclass> allPClass = pclassService.getAllPclass();
        model.addAttribute("list",allPClass);
        return "student/register";
    }

    @PostMapping("/CanRegister")
    public String register(Users users,String username,String  userpwd,String truename,Integer classid,HttpServletRequest request){
        Users byName = usersService.getByName(username);
        users.setUsername(username);
        users.setUserpwd(userpwd);
        users.setTruename(truename);
        users.setClassid(classid);
        usersService.addUsers(users);
        if (byName==null){
            if (userpwd.equals("")){
                request.getSession().setAttribute("msg1","密码为空");
                return "redirect:/register";
            }
            if (truename.equals("")){
                request.getSession().setAttribute("msg2","真实姓名为空");
                return "redirect:/register";
            }
            if (username.equals("")){
                request.getSession().setAttribute("msg3","用户名为空");
                return "redirect:/register";
            }
            System.out.println("用户可以注册");
            return "redirect:/toStuLogin";
        }else {
            System.out.println("用户已存在");
            return "redirect:/register";
        }

    }

    //修改密码
    @PostMapping("/updateStuPwd")
    public String updateUserPwd(String userpwd,Integer userid){
        Integer i = usersService.updUserPwd(userpwd,userid);
        if(i==0){
            return "";
        }
        else{
            return "redirect:/toStuLogin";
        }
    }

    //退出登录
    @GetMapping("/logout")
    public String slogout(HttpServletRequest request){
        request.getSession().removeAttribute("lis");
        request.getSession().removeAttribute("truename");
        return "redirect:/toStuLogin";
    }

}
